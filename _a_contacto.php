			<div class="container">
                <div class="row">
                    <div class="title">
                       <h1>CONTACTO</h1> 
                    </div>
                    <p id="contact-response" style="font-size: 25px">
                        Deja tu mensaje y enseguida nos pondremos en contacto contigo
                    </p>
                    <div class="contact-form col-md-10 col-md-offset-1 col-sm-12 wow fadeInUp" data-wow-delay=".2s">
                        <form action="processForm.php" id="contact-form" method="post">
                            <label class="input col-xs-4 col-vs-12">
                                <input name="name" placeholder="Nombre Completo" type="text">
                                    <span class="icon">
                                    </span>
                                </input>
                            </label>
                            <label class="input col-xs-4 col-vs-12">
                                <input name="email" placeholder="Correo Electrónico" type="text">
                                    <span class="icon">
                                    </span>
                                </input>
                            </label>
                            <label class="input col-xs-4 col-vs-12">
                                <input name="phone" placeholder="Número Telefonico" type="text">
                                    <span class="icon">
                                    </span>
                                </input>
                            </label>
                            <div class="clearfix">
                            </div>
                            <label class="textarea col-lg-12">
                                <textarea cols="30" name="message" placeholder="Mensaje" rows="10">
                                </textarea>
                            </label>
                            <div class="button" id="submit-contact">
                                <button type="submit">
                                    Enviar
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>