<?php header ('Content-type: text/html; charset=utf-8'); ?>
<!DOCTYPE html>
<html lang="es">
<meta http-equiv="content-type" content="text/html; charset=UTF-8" />
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <!-- CSS STYLE -->
    <link rel="stylesheet" type="text/css" href="css/bootstrap.css" media="screen" />
    <link rel="stylesheet" type="text/css" href="style.css" media="screen" />
    <link rel="stylesheet" type="text/css" href="css/icomoon.css" media="screen" />
    <link rel="stylesheet" type="text/css" href="css/owl.carousel.css" media="screen" />
    <link rel="stylesheet" type="text/css" href="css/magnific-popup.css" media="screen" />
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.6.6/sweetalert2.css"  />
    <link rel="stylesheet" type="text/css" href="http://fontawesome.io/assets/font-awesome/css/font-awesome.css"  />
    <link rel="stylesheet" type="text/css" href="css/animate.css">
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.6.6/sweetalert2.js"></script>
    <!--[if lt IE 9]> <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script> <script src="http://css3-mediaqueries-js.googlecode.com/svn/trunk/css3-mediaqueries.js"></script> <![endif]-->
    <title>BIENVENIDO | CAPITALA</title>
</head>

<body onload="hide_preloader();" class="home">
<style type="text/css">
    body{
      margin-left: auto;
      margin-right: auto;
      overflow-x: hidden;
    }
    .title h1{
      min-width: 340px !important;
    }
    .img_valor_logo{
      margin-left: 0px;
      /* max-width: 180px; */
      max-width: 100px;
      margin-top: 0px;
    }
    .separador_{
      padding-top: 30px;
      padding-bottom: 30px;
    }
</style>
<!-- Page wrap -->
<div id="page-wrap">
    <!-- Preloader -->
        <div id="preloader">
        <img src="images/parallax/logo.png" width="71px" alt="">
        <div class="spinner">
            <div class="rect1"></div>
            <div class="rect2"></div>
            <div class="rect3"></div>
            <div class="rect4"></div>
            <div class="rect5"></div>
        </div>
        </div>
        <!-- CONTENIDO -->
        <header>
           <div class="container">
              <div class="row">
                 <h1 class="logo"><a href="./"><img src="images/parallax/logo.png" alt="logo"></a></h1>
                 <div class="menu-mobile">
                    <p></p>
                 </div>
                 <nav class="menu">
                    <ul>
                       <li><a href="index.php#work">¿QUI&Eacute;NES SOMOS?</a></li>
                       <li><a href="index.php#valor_adicional_1">VALOR ADICIONAL</a></li>
                       <li><a href="index.php#fideicomiso">Fideicomiso</a></li>
                       <li><a href="index.php#proyectos">PROYECTOS</a></li>
                       <li><a href="index.php#contact">CONTACTO</a></li>
                    </ul>
                 </nav>
              </div>
           </div>
        </header>
        <!-- <div class="background-over"></div> -->
        <div class="banner fullscreen-banner" id="banner" data-stellar-background-ratio="0.1"
        style="background-image: url(''); background-repeat: no-repeat; background-attachment: fixed; background-position: center; ">
          <img width="100%" src="images/capitala_01.png">
           <!--
           <div class="scroll-s1">
              <div class="icon"><img src="images/scroll-s1.png" alt=""></div>
           </div>
           -->
        </div>
        <style type="text/css">
            .texto_porcentaje{
              text-align: center;
              color: #fff;
              font-style: normal;
            }
            .image_fidei{
              padding-left: 10px !important;
              padding-right: 10px !important;
            }
            .fadeInLeft{
              margin-top: 0px;
            }
            .list-tam1,.list-tam2{
              margin-top: 0px;
            }
            .fidei_h1{
              height: inherit;
            }
            .fidei_show{
              position: inherit;
              margin-left: inherit;
              top: inherit;
            }
            .fidei_show2{
              top: auto;
              position: inherit;
            }
            .contacto_ajuste{
              margin-left: 93px;
            }
            #_actinver{
              float: right;
              margin-left: 5px;
            }
            #bg-parallax2 .ct .box {
              display: block;
            }
            .text{
              margin-bottom: 60px;
            }

            @media only screen and (min-width:960px){
            /* styles for browsers larger than 960px; */
              .texto_porcentaje{
                font-size: 60px;
              }
              .text_1_img{
                margin-left: 125px !important;
              }
              .list_proyects{
                margin-top: -70px;
              }
            }
             @media only screen and (min-width:1024px){
                /* styles for browsers larger than 1440px; */
              .texto_porcentaje{
                font-size: 72px;
              }
              .text_1{
                margin-left: 300px !important;
              }
              .image_fidei{
                margin-left: 100px;
              }
            }
            @media only screen and (min-width:1440px){
                /* styles for browsers larger than 1440px; */
              .texto_porcentaje{
                font-size: 72px;
              }
              .text_1{
                margin-left: 200px !important;
              }
            }
            @media only screen and (min-width:2000px){
                /* for sumo sized (mac) screens */
              .texto_porcentaje{
                font-size: 72px;
              }
            }
            @media only screen and (max-device-width:480px){
               /* styles for mobile browsers smaller than 480px; (iPhone) */
              .texto_porcentaje{
                font-size: 20px;
              }
              .image_fidei{
                width: 100%;
                padding: 20px;
              }
              .fadeInLeft{
                margin-top: 180px;
              }
              .list-tam1{
                margin-top: 195px !important;
              }
              .list-tam2{
                margin-top: 220px !important;
              }
              .fidei_h1{
                height: 115px !important;
              }
              .fidei_show{
                display: block;
                position: absolute;
                margin-left: 170px !important;
                top: 25px !important;
              }
              .fidei_show2{
                top: -15px;
                position: relative;
              }
              .contacto_ajuste{
                    margin-left: -15px !important;
              }
              #_actinver{
                margin-right: 95px;
                margin-top: -15px;
              }
              #bg-parallax2 .ct .box{
                display: none !important;

              }
              .text{
                /* margin-bottom: -35px !important; */
              }
              .list_proyects{
                padding-top: 0px !important;
              }
            }
            @media only screen and (device-width:768px){
               /* default iPad screens */
               .texto_porcentaje{
                font-size: 20px;
              }

              .image_fidei{
                width: 45%;
                margin-left: 25px
              }
            }

            /* different techniques for iPad screening */
            @media only screen and (min-device-width: 481px) and (max-device-width: 1024px) and (orientation:portrait) {
              /* For portrait layouts only */
              .texto_porcentaje{
                font-size: 35px;
              }
            }

            @media only screen and (min-device-width: 481px) and (max-device-width: 1024px) and (orientation:landscape) {
              /* For landscape layouts only */
              .texto_porcentaje{
                font-size: 35px;
              }
            }
          .timeline li .list-content p{
            font-weight: 100;
            text-align: left;
          }
          .lasted-post .box .inner{
            height: 477px;
          }
        </style>
        <div id="main-content">
          <div class="scroll-s1" style="top: 60px; position: relative; ">
            <div class="icon"><img src="images/scroll-s1.png" alt=""></div>
          </div>
           <div id="work" class="va-work" style="padding-top: 120px;">
              <div class="container">
                 <div class="row">
                    <div class="title">
                       <h1>¿Qui&eacute;nes somos?</h1>
                    </div>
                     <p class="text">Capitala nace de la fusión de un grupo de desarrolladores 100% mexicanos, con amplia experiencia en la industria de la construcción y desarrollo de proyectos inmobiliarios, con el propósito de llevar al mercado instrumentos de inversión para el público en general.</p>
                     <div class="timeline" style="font: 0.9em/0.5em 'NeutraText_BookAlt';">
                       <ul>
                          <li class="col-sm-7 col-xs-11 col-vs-12 wow fadeInLeft" style="margin-top: 0px;" data-wow-delay=".2s">
                             <div class="inner">
                                <div class="list-content">
                                   <h3 class="year">2004</h3>
                                   <!-- <h2>Tom meet robert then...</h2>
                                   <a href="#"><span class="icofont moon-cube-3"></span></a> -->
                                   <p>Fundaci&oacute;n de CMC</p>
                                </div>
                             </div>
                          </li>
                          <li class="col-sm-7 col-xs-11 col-vs-12 wow fadeInRight" data-wow-delay=".2s">
                             <div class="inner">
                                <div class="list-content">
                                   <h3 class="year">2006</h3>
                                   <!-- <h2>Tom meet robert then...</h2>
                                   <a href="#"><span class="icofont moon-trophy-star"></span></a>-->
                                   <p>
                                   • Nicol&aacute;s Romero<br> • Conjunto Desierto de los Leones
                                   </p>
                                </div>
                             </div>
                          </li>
                          <li class="col-sm-7 col-xs-11 col-vs-12 col-vs-12 wow fadeInLeft" data-wow-delay=".2s">
                             <div class="inner">
                                <div class="list-content">
                                   <h3 class="year">2008</h3>
                                   <p>
                                    • Lago Victoria<br> • Lomas de Angel&oacute;polis</p>
                                </div>
                             </div>
                          </li>
                          <li class="col-sm-7 col-xs-11 col-vs-12 wow fadeInRight" data-wow-delay=".2s">
                             <div class="inner">
                                <div class="list-content">
                                   <h3 class="year">2009</h3>
                                   <p>
                                   • Cluster Lomas de Angel&oacute;polis II Secci&oacute;n <br>• Porfirio D&iacute;az</p>
                                </div>
                             </div>
                          </li>
                          <li class="col-sm-7 col-xs-11 col-vs-12">
                             <div class="inner">
                                <div class="list-content list-tam1" >
                                   <h3 class="year">2011</h3>
                                   <p>• Condominio Lomas en Angel&oacute;polis <br>• Cerro Gordo<br>• Penthouse Bosque Real
<br>• Corvaglia</p>
                                </div>
                             </div>
                          </li>
                          <li class="col-sm-7 col-xs-11 col-vs-12">
                             <div class="inner">
                                <div class="list-content">
                                   <h3 class="year">2012</h3>
                                   <p>• Peugeot Sat&eacute;lite<br>• Real Inn<br>• Servicio Panamericano de Valores (Morelia)</p>
                                </div>
                             </div>
                          </li>
                          <li class="col-sm-7 col-xs-11 col-vs-12">
                             <div class="inner">
                                <div class="list-content list-tam1">
                                   <h3 class="year">2013</h3>
                                   <p>• Servicio Panamericano de Valores (Cd. Victoria) <br>• Escand&oacute;n<br>• Colegio Vallarta<br>• S&eacute;neca 61<br>• Parker & Lennox<br>• Planta de Cemento Moctezuma (Toluca)</p>
                                </div>
                             </div>
                          </li>
                          <li class="col-sm-7 col-xs-11 col-vs-12">
                             <div class="inner">
                                <div class="list-content">
                                   <h3 class="year">2014</h3>
                                   <p>• Nabor Carrillo
                                  <br>• Leandro Valle
                                  <br>• La Palma
                                  <br>• Servicio Panamericano de Valores (Iguala)
                                  <br>• Tehuantepec 170</p>
                                </div>
                             </div>
                          </li>
                          <li class="col-sm-7 col-xs-11 col-vs-12">
                             <div class="inner">
                                <div class="list-content list-tam2">
                                   <h3 class="year">2015</h3>
                                   <p>• San Jer&oacute;nimo 1090 <br>• Condominio La Perita</p>
                                </div>
                             </div>
                          </li>
                          <li class="col-sm-7 col-xs-11 col-vs-12">
                             <div class="inner">
                                <div class="list-content">
                                   <h3 class="year">2017</h3>
                                   <p>• Ju&aacute;rez 89
                                  <br>• Privada del Romance Bosque Real
                                  <br>• San Bernab&eacute; 654</p>
                                </div>
                             </div>
                          </li>
                       </ul>
                       <a class="loadmore" href="#">loadmore</a>
                    </div>
                 </div>
              </div>
           </div>
           <style type="text/css">
            .list_proyects li{
              font-size: 15px;
            }
            .ct .icofont span {
              background-image: none;
            }
           </style>
           <!-- termina -->
           <!-- PARALLAX 2 -->
           <!-- Bg parallax 2 -->
          <div data-stellar-background-ratio="0.1" id="bg-parallax2" style="margin-top: -20px;">
            <div class="parallax-over"> </div>
            <div class="container">
                <style type="text/css">
                    .img_1{
                        background-image: url('images/capitala_01.png_');
                    }
                    .imagen{
                        margin: 0;
                        width: 100%;
                        display:inline-block;
                        height: 75%;
                        float:none;
                        color: #ffffff;
                    }
                    .icofont_img {
                          width: 80%;
                    }
                </style>
                <div id="valor_adicional_1" class="row">
                    <div class="title">
                       <h1>VALOR ADICIONAL</h1>
                    </div>
                    <p class="text">CAPITALA cuenta con socios reconocidos que hacen de este fondo de inversión el más completo, tanto para los inversionistas, como para los clientes.</p>
                    <div class="ct">
                        <div class="box col-sm-2 col-xs-6 col-vs-12">
                            <div class="icofont">
                              <img class="icofont_img" src="images/valor/1.png" style="margin-top: 15px;"  >
                            </div>
                            <p class="more-ct" style="font-size: 10px;">
                                Desarrollo Inmobiliario
                            </p>
                        </div>
                        <div class="box col-sm-2 col-xs-6 col-vs-12">
                            <div class="icofont">
                              <img class="icofont_img" src="images/valor/2.png" style="width: 80px; margin-top: 10px;" >
                            </div>
                            <p class="more-ct" style="font-size: 10px;">
                                Comercialización
                            </p>
                        </div>
                        <div class="box col-sm-2 col-xs-6 col-vs-12">
                            <div class="icofont">
                              <img class="icofont_img" src="images/valor/3.png" style="margin-top: 6px;" >
                            </div>
                            <p class="more-ct" style="font-size: 10px;">
                                SupervisIón Legal, Corporativa, Contable y Fiscal
                            </p>
                        </div>
                        <div class="box col-sm-2 col-xs-6 col-vs-12">
                            <div class="icofont">
                              <img class="icofont_img" src="images/valor/4.png" >
                            </div>
                            <p class="more-ct" style="font-size: 10px;">
                                Administración de Capitales
                            </p>
                        </div>
                        <div class="box col-sm-2 col-xs-6 col-vs-12">
                            <div class="icofont">
                              <img class="icofont_img" src="images/valor/5.png" style=" width: 70%;">
                            </div>
                            <p class="more-ct" style="font-size: 10px;">
                                Supervisión de Obra Externa
                            </p>
                        </div>
                        <div class="box col-sm-2 col-xs-6 col-vs-12">
                            <div class="icofont">
                              <img class="icofont_img" src="images/logo_3_.png" style="width: 100%; margin-top: -15px;">
                            </div>
                            <p class="more-ct" style="font-size: 10px;">
                                Único fondo con experiencia en las funciones principales
                            </p>
                        </div>
                    </div>
                    <div class="list_proyects">
                      <div class="ct" style="margin-bottom: -25px;">
                         <figure class="col-sm-4 wow fadeInUp" data-wow-delay=".2s">
                            <div class="icofont">
                              <img class="img_valor_logo" src="images/logo/5_logo.png">
                            </div>
                            <figcaption>
                               <div class="tt">
                                  <h3>ACTINVER <br><small style="color: #444;">Fiduciario</small></h3>
                               </div>
                               <p class="text">
                                <li>Institución reconocida con amplia experiencia en manejo de fideicomisos como FIBRAS y CKDs.</li>
                                <li>Control sobre el destino de las inversiones.</li>
                                <li>Claridad en cuentas.</li>
                                <li>Reporte y atención para inversionistas.</li>
                              </p>
                            </figcaption>
                         </figure>
                         <figure class="col-sm-4 wow fadeInUp" data-wow-delay=".4s">
                            <div class="icofont">
                              <img class="img_valor_logo" src="images/logo/7_logo.png"
                              style="margin-top: 1px; margin-right: 0px;">
                            </div>
                            <figcaption>
                               <div class="tt">
                                  <h3>PROMOTORA AURA <br><small style="color: #444;">Administraci&oacute;n</small></h3>
                               </div>
                               <p>
                                <li>Operación y control administrativo, legal, contable y fiscal del fideicomiso.</li>
                                <li>Administración y optimización de los activos.</li>
                                <li>Estructura comercial para garantizar el máximo rendimiento de la inversión.</li>
                                <li>Búsqueda contínua de oportunidades en el mercado.</li>
                               </p>
                            </figcaption>
                         </figure>
                         <figure class="col-sm-4 wow fadeInUp" data-wow-delay=".6s">
                            <div class="icofont">
                              <img class="img_valor_logo" src="images/logo/6_logo.png" style="margin-top: 2px;  margin-left: 3px;">
                            </div>
                            <figcaption>
                               <div class="tt">
                                  <h3>INCA <br><small style="color: #444;">Supervisi&oacute;n de obra</small></h3>
                               </div>
                               <p>
                                <li>Miembro de Comité de Inversión y Comité Técnico.</li>
                                <li>Control de presupuesto y de programa.</li>
                                <li>Supervisión de todos los desarrollos inmobiliarios.</li>
                                <li>Reporte periódico por proyecto.</li>
                                <li>Calidad en producto terminado.</li>
                               </p>
                            </figcaption>
                         </figure>
                      </div>

                        <div class="link" style="margin-top: 50px;">
                          <span class="contact-now" style="font-style: normal; margin-bottom: 50px;">¿Tienes dudas? Contáctanos ahora!</span>
                          <a href="./index.php#contact">Contáctanos</a>
                        </div>
                    </div>
                </div>
            </div>
          </div>
          <div id="rendimiento_esperado" style="padding-top: 30px; background: rgba(173, 125, 125, 1);">
              <div class="container">
                 <div class="row">
                    <div class="title">
                      <img src="images/logo_2.png" style=" width: 30%;">
                    </div>
                      <p class="text texto_porcentaje">RENDIMIENTO MÍNIMO ANUAL ESPERADO: 21%</p>
                 </div>
              </div>
          </div>
          </div>
          <div id="fideicomiso" style="padding-top: 65px;">
              <div class="container">
                 <div class="row">
                    <div class="title" style="padding-left: 14px; padding-right: 14px;">
                       <h1 class="fidei_h1">Fideicomiso Capitala <span id="_actinver"> - Actinver</span></h1>
                    </div>
                    <p class="text">El fideicomiso garantiza velar por los intereses de los inversionistas, disminuyendo el riesgo en los proyectos y buscando una transparencia total en cada negocio.</p>
                    <style type="text/css">
                      .text_1{
                        text-align: left;
                        float: left;
                        color: #7f7f7f;
                        font-weight: 100;
                        margin-left: 15px;
                        width: 90%;
                      }
                      .text_2{
                        font-size: 20px;
                        text-transform: uppercase;
                      }
                      .text_3{
                        text-transform: capitalize;
                        font-size: 15px;
                      }
                      .fle_col{
                        color: rgba(173, 125, 125, 1);
                      }
                    </style>
                    <p class="text text_1" style="margin-bottom: 40px; margin-top: 0px; ">
                    <span class="text_2"><b class="fle_col">‣</b> TASA
                      <span style="font-size: 13px;">preferente a fideicomisarios en primer lugar:</span>
                      <span class="text_3">certificados A 12% anual</span>
                    </span>
                    <br>
                    <span class="text_2"><b class="fle_col">‣</b>
                      <span style="font-size: 13px;"> Rendimiento mínimo anual por </span>proyecto: <b style="color:#d4a450">30%</b>
                    </span>
                    <br><br>
                       <img class="text_1_img" src="images/valor/re_esp.png">
                    <br><br>
                    <span class="text_2"><b class="fle_col">‣</b>
                      <span style="font-size: 13px;"> Rendimiento mínimo anual esperado para los </span>inversionistas
                      <span style="font-size: 13px;">a :</span>
                      <span style="color:#d4a450">21%</span>
                    </span>
                    <br>
                    <span class="text_2"><b class="fle_col">‣</b>
                      <span style="font-size: 13px;"> Valor del </span>Fondo :
                      <span class="text_3">300MDP</span>
                    </span>
                    <br>
                    <span class="text_2"><b class="fle_col">‣</b> Administrador
                      <span class="text_3" style="text-transform: none;">Promotora Aura aporta el 20% del valor del fideicomiso.</span>
                    </span>
                    </p>
                    <img class="image_fidei" src="images/parallax/bg5_capa-1.png">
                    <img class="image_fidei" src="images/parallax/bg5_capa-2.png">
                 </div>
              </div>
          </div>

          <div id="proyectos" class="lasted-post" style="margin-top: -135px; padding-bottom: 50px;">
              <div class="container">
                 <div class="row">
                    <div class="title">
                       <h1 style="margin-top: 80px;">PROYECTOS ENTREGADOS</h1>
                    </div>
                    <p class="text" style="text-align: left; margin-top: -20px;"></p>
                    <div class="ct wow fadeInUp" data-wow-delay=".2s">
                        <div id="owl-lasted" class="slide1">
                             <?php 
                             for($i=1;$i<=4;$i++)
                             {
                              switch ($i) {
                                case "1":
                                  $image_h = "images/proyectos/Por.jpg";
                                  $title_h = "Porfirio Díaz";
                                  $descripcion = "Desarrollo de 6 viviendas residenciales plus ubicado en la Colonia San Jerónimo Lídice, al sur de la Ciudad de México. Todas las casas se vendieron en preventa.";
                                  $home = "6 Viviendas";
                                  $fecha = "2015";
                                break;
                                case "2":
                                  $image_h = "images/proyectos/Her.jpg";
                                  $title_h = "Heriberto Frías";
                                  $descripcion = "Cuatro departamentos de lujo ubicados en la Colonia del Valle con excelentes vías de comunicación. Vendidos en 4 meses.";
                                  $home = "4 Departamentos";
                                  $fecha = "2016";
                                break;
                                case "3":
                                  $image_h = "images/proyectos/Jua.jpg";
                                  $title_h = "Juárez";
                                  $descripcion = "Dos residencias de lujo ubicadas en la Calle de Juárez, San Jerónimo, al sur de la Ciudad de México. Vendidas en preventa en menos de 2 meses.";
                                  $home = "2 Residencias";
                                  $fecha = "2017";
                                break;
                                case "4":
                                  $image_h = "images/proyectos/Jer.jpg";
                                  $title_h = "San Jerónimo";
                                  $descripcion = "Desarrollo de 7 viviendas residenciales plus ubicado en San Jerónimo, al sur de la Ciudad de México. En menos de un año las casas tuvieron una plusvalía del 25%.";
                                  $home = "7 Viviendas";
                                  $fecha = "2017";
                                break;
                              }
                              ?>
                              <div class="item">
                                  <div class="box">
                                      <div class="inner">
                                          <div class="image">
                                          <!-- src="http://via.placeholder.com/350x200" -->
                                              <img alt="" src="<?php echo $image_h; ?>" >
                                                  <a class="zoom" href="proyecto.php?p=<?php echo $i; ?>">
                                                      <span class="icofont moon-plus">
                                                      </span>
                                                  </a>
                                          </div>
                                          <div class="caption">
                                              <h3>
                                                  <a href="#">
                                                      <?php echo $title_h; ?>
                                                  </a>
                                              </h3>
                                              <p>
                                                  <?php echo $descripcion; ?>
                                              </p>
                                          </div>
                                          <div class="w-info">
                                              <span>
                                                  <span class="icofont moon-clock">
                                                  </span>
                                                  <a href="#">
                                                      <?php echo $fecha; ?>
                                                  </a>
                                              </span>
                                              <span>
                                                  <span class="icofont moon-home">
                                                  </span>
                                                  <a href="#">
                                                      <?php echo $home; ?>
                                                  </a>
                                              </span>
                                          </div>
                                      </div>
                                  </div>
                              </div>
                              <?php
                             }
                             ?>
                        </div>
                    </div>
                 </div>

              </div>
           </div>

           <div id="proyectos" class="lasted-post" style="padding-top: 15px; padding-bottom: 65px;">
              <div class="container">
                 <div class="row">
                    <div class="title">
                       <h1>PRÓXIMOS PROYECTOS</h1>
                    </div>
                    <div class="ct wow fadeInUp" style="margin-top: 45px;" data-wow-delay=".2s">
                        <div id="owl-lasted" class="slide2" >
                             <?php
                             for($i=5;$i<=7;$i++)
                             {
                              switch ($i) {
                                case "5":
                                  $image_h = "images/prox_proyectos/bosque_real/01.png";
                                  $title_h = "Bosque Real";
                                  $descriocion = "28 departamentos de lujo ubicados en el fraccionamiento Bosque Real. Todos cuentan con jardín propio y terraza";
                                  $fecha = "Febrero 2019";
                                break;
                                case "6":
                                  $image_h = "images/prox_proyectos/bosque_bravo/1.png";
                                  $title_h = "Bosque Bravo";
                                  $descriocion = "Desarrollo en construcción de 16 residencias ubicadas en Avándaro, Valle de Bravo. Se inició la preventa en Julio de 2017.";
                                  $fecha = "Octubre 2018";
                                break;
                                case "7":
                                  $image_h = "images/parallax/proximamente.jpg";
                                  $title_h = "San Bernabé";
                                  $descriocion = "Cinco residencias plus ubicadas en la zona sur de la Ciudad de México.";
                                  $fecha = "Octubre 2017";
                                break;
                              }
                              ?>
                              <div class="item">
                                  <div class="box">
                                      <div class="inner">
                                          <div class="image">
                                              <img alt="" src="<?php echo $image_h; ?>">
                                              <a class="zoom" href="proyecto.php?p=<?php echo $i; ?>">
                                                  <span class="icofont moon-plus">
                                                  </span>
                                              </a>
                                          </div>
                                          <div class="caption">
                                              <h3>
                                                  <a href="#">
                                                      <?php echo $title_h; ?>
                                                  </a>
                                              </h3>
                                              <p>
                                                  <?php echo $descriocion; ?>
                                              </p>
                                          </div>
                                          <div class="w-info">
                                              <span>
                                                  <span class="icofont moon-clock">
                                                  </span>
                                                  <a href="#">
                                                      <?php echo $fecha; ?>
                                                  </a>
                                              </span>
                                          </div>
                                      </div>
                                  </div>
                              </div>
                              <?php
                             }
                             ?>
                        </div>
                    </div>
                 </div>

              </div>
           </div>

          <div class="contacting" data-stellar-background-ratio="0.1" id="contact">
            <div class="contact-overlay">
            </div>
            <div class="container">
                <div class="row">
                    <div class="title">
                       <h1>CONTACTO</h1>
                    </div>
                    <p id="contact-response" style="font-size: 25px">
                        Deja tu mensaje y enseguida nos pondremos en contacto contigo
                    </p>
                    <div class="contact-form col-md-10 col-md-offset-1 col-sm-12 wow fadeInUp contacto_ajuste" data-wow-delay=".2s">
                        <form style="margin-left: 0px;" >
                            <label class="input col-xs-4 col-vs-12">
                                <input name="name" id="f_nombre" placeholder="Nombre Completo" type="text">
                                    <span class="icon"></span>
                                </input>
                            </label>
                            <label class="input col-xs-4 col-vs-12">
                                <input name="email" id="f_correo" placeholder="Correo Electrónico" type="text">
                                    <span class="icon"></span>
                                </input>
                            </label>
                            <label class="input col-xs-4 col-vs-12">
                                <input name="phone" id="f_telefono" placeholder="Número Telefonico" type="text">
                                    <span class="icon"></span>
                                </input>
                            </label>
                            <div class="clearfix">
                            </div>
                            <label class="textarea col-lg-12">
                                <textarea cols="30" id="f_mensaje" name="message" placeholder="Mensaje" rows="10"></textarea>
                            </label>
                            <div class="button" id="submit-contact">
                                <button type='button' onclick="validarCampos();">Enviar</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <footer>
           <div class="container">
              <div class="row" style="    margin-top: 120px;">
                 <div id="scroll-top" style="margin-top: 60px;"><div class="icon" style="position: absolute;left: 15px;top: 12px;"><img src="images/scroll-s1.png" alt="" style="
    transform: rotate(135deg);
"></div></div>
                 <div class="phone col-xs-4 col-vs-12">
                    <h2><span class="icofont moon-phone-3"></span></h2>
                    <p><a href="tel:+5552793550" style="color:#fff; ">+52 (55) 52793550</a></p>
                 </div>
                 <div class="address col-xs-4 col-vs-12">
                    <h2><span class="icofont moon-location-3"></span></h2>
                    <p>Homero 823, Col. Polanco, CP. 11550, CDMX, México</p>
                 </div>
                 <div class="mail col-xs-4 col-vs-12">
                    <h2><span class="icofont moon-envelop-2"></span></h2>
                    <p>
                       <a href="mailto:info@capitala.mx">info@capitala.mx</a>
                    </p>
                 </div>
                 <div class="clearfix"></div><br><br>
                 <style type="text/css">
                   .aviso_privacidad{
                    font-size: 15px;
                    color: #FFF;
                   }
                   .aviso_privacidad:hover{
                    color: #ad7d7d;
                   }
                 </style>
                 <div class="col-lg-12 text-center">
                 <a href="#myModal" data-toggle="modal" data-target="#myModal" class="aviso_privacidad">
                 Aviso de Privacidad </a>
                 </div>
                 <br><br>All Rights Reserved © 2017
              </div>
           </div>
        </footer>
        </div>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
        <script type="text/javascript" src="js/jquery.custom.js"></script>
        <script type="text/javascript" src="js/jquery.nav.js"></script>
        <script type="text/javascript" src="js/jquery.owl.carousel.js"></script>
        <script type="text/javascript" src="js/jquery.stellar.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <script type="text/javascript">
        function validarCampos(){
          var f_nombre        = document.getElementById('f_nombre').value;
          var f_correo        = document.getElementById('f_correo').value;
          var f_telefono      = document.getElementById('f_telefono').value;
          var f_mensaje       = document.getElementById('f_mensaje').value;

          /* SAVE DATA */
          var dataFormResultados = new FormData();
                dataFormResultados.append("f_nombre", f_nombre);
                dataFormResultados.append("f_correo", f_correo);
                dataFormResultados.append("f_telefono", f_telefono);
                dataFormResultados.append("f_mensaje", f_mensaje);

                var requestFormResultados = new XMLHttpRequest();
                requestFormResultados.onreadystatechange = function(){
                    if(requestFormResultados.readyState == 4){
                        try {
                            var resp = JSON.parse(requestFormResultados.response);
                        } catch (e){
                            var resp = {
                                status: 'error',
                                dataFormResultados: 'Unknown error occurred: [' + requestFormResultados.responseText + ']'
                            };
                        }
                        if(resp.status == 'enviado'){
                         swal(
                            'Enviado!',
                            'Tu mensaje ha sido enviado con exito!',
                            'success'
                          );
                        }else{
                          swal(
                            'Algo Ocurrio!',
                            'Completa todos los campos correctamente e intenta de nuevo!',
                            'error'
                          );
                        }

                        console.log(resp.status + ' : ' + resp.dataFormResultados);
                    }
                };
                requestFormResultados.open('POST', 'sendmail.php');
                requestFormResultados.send(dataFormResultados);

        }
        </script>
        <!--
        <script type="text/javascript" src="js/jquery.ytplayer.js"></script>
        <script type="text/javascript" src="js/jquery.appear.min.js"></script>
        <script type="text/javascript" src="js/jquery.countTo.js"></script>
        <script type="text/javascript" src="js/jquery.magnific-popup.min.js"></script>
        <script type="text/javascript" src="js/jquery.wow.min.js"></script>
        <script type="text/javascript" src="js/script-parallax-video.js"></script>
        <script type="text/javascript" src="js/jquery.isotope.min.js"></script>
        -->
    <!-- END CONTENIDO -->
</body>
<style type="text/css">
  .modal-dialog {
    width: 90%;
    margin-top: 100px;
    font-family: Arial,Helvetica Neue,Helvetica,sans-serif;
  }
  .modal-dialog h4{
    font-size: 15px;
  }
  .modal-dialog .modal-body{
    font-size: 14px;
  }
  .modal-dialog .modal-body h2{
    margin-top: 15px;
    margin-bottom: 10px;
    text-transform: uppercase;
  }
  .modal-dialog .modal-body h3 ,.modal-dialog .modal-body .title_text,.modal-dialog .modal-body p {
    margin-top: 10px;
    margin-bottom: 5px;
    margin-left: 20px;
  }
  .modal-dialog .modal-body p span{
    margin-left: 0px !important;
  }
  .modal-dialog .modal-body li{
    margin-left: 25px;
  }
  .modal-dialog .modal-body h2,.modal-dialog .modal-body .title_text{
    font-weight: bold !important;
  }
  .modal-dialog .modal-body .lista{
    list-style: none;
    margin-left: 40px;
  }
</style>
<!-- Aviso de Privacidad -->
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title"><strong>Aviso de Privacidad Integral</strong></h4>
      </div>
      <div class="modal-body">
        <p>En cumplimiento a lo establecido en la Ley Federal de Protección de Datos Personales en Posesión de los Particulares, su Reglamento y los Lineamientos del Aviso de Privacidad, <strong>Promotora Aura Capital, S.C. (CAPITALA)</strong>, con domicilio en Homero 823, Colonia Polanco, Delegación Miguel Hidalgo, C.P. 11550, Ciudad de México (el “<strong>Responsable</strong>”), pone el presente Aviso de Privacidad (el “<strong>Aviso</strong>”) a disposición de cualquier persona física respecto de la cual trate sus datos personales (el “<strong>Titular</strong>”), .</p>

<h2>I.  Datos Personales. </h2>

<h3>El <strong>Responsable</strong> podrá tratar los siguientes datos personales del <strong>Titular</strong>: </h3>

<span class="title_text">a)  Inversionistas.</span>
    <li class="lista">• Identificación / Contacto / Patrimoniales / Financieros: nombre, domicilio, correo electrónico, número telefónico, número de teléfono celular, números de cuenta bancaria, profesión, oficio, firma autógrafa, así como Registro Federal de Contribuyentes (RFC).</li>



<span class="title_text">b)  Empleados.</span>

    <li class="lista">• Identificación / Contacto / Académicos / Laborales / Patrimoniales / Financieros: nombre, domicilio, correo electrónico, número telefónico, número de teléfono celular, firma autógrafa, Registro Federal de Contribuyentes (RFC), Clave Única de Registro de Población (CURP), nivel de estudios, título, cédula profesional, puesto, referencias laborales, número de seguridad social, créditos contratados con entidades gubernamentales (INFONAVIT o FONACOT) y número de cuenta bancaria.</li>

    <p>Los datos personales patrimoniales o financieros del <strong>Titular</strong> serán recabados únicamente cuando sea necesarios para dar cumplimiento a las obligaciones derivadas de la relación que el <strong>Titular</strong> mantiene con el <strong>Responsable</strong>. </p>

    <p>El <strong>Responsable</strong> manifiesta que para las finalidades indicadas en el presente Aviso, no recaba datos personales sensibles del <strong>Titular</strong>.</p>

    <h2>II. FINALIDADES.</h2>

    <h3>El <strong>Responsable</strong> podrá tratar los datos personales del <strong>Titular</strong> para las siguientes finalidades: </h3>

    <p><span class="title_text">a)  Inversionistas: </span> (i) identificación de posibles inversionistas; (ii) confirmación de la procedencia de los recursos a invertir por parte de el <strong>Titular</strong>; (iii) informar al agente fiduciario cualquier dato que considere neceario o relevante con respecto al <strong>Titular</strong>, y (iv) cumplir las obligaciones contraídas con o por el <strong>Titular</strong>. </p>

    <p><span class="title_text">b)  Empleados: </span>(i) efectuar el proceso de selección y reclutamiento de personal; (ii) integrar el expediente laboral del <strong>Titular</strong> en caso de contratación; y (iii) dar cumplimiento a las prestaciones y obligaciones derivadas de la relación laboral con el <strong>Titular</strong>.</p>

    <p>En los casos en que el <strong>Responsable</strong> pretenda tratar los datos personales para una finalidad distinta a las contenidas en el presente Aviso de Privacidad, el <strong>Responsable</strong> se compromete a hacerlo del conocimiento del <strong>Titular</strong> y a obtener su consentimiento para el nuevo tratamiento.</p>

    <p>En  relación  con  los  datos  personales  que  se  mencionan  en los  párrafos anteriores, Capitala se  compromete  a  que  el  tratamiento  será  el absolutamente indispensable para las finalidades mencionadas y  serán tratados  con las medidas de seguridad adecuadas para proteger la confidencialidad de los mismos y que en caso de prever otra finalidad se le solicitará su autorización efectuando la modificación al Aviso de Privacidad respectivo.</p>

    <p>Al proporcionar a Capitala información que constituya Datos Personales se entenderá que usted otorga también su consentimiento para el tratamiento de los mismos de conformidad con el presente Aviso de Privacidad.</p>

    <h2>III.  CONSENTIMIENTO.</h2>

    <p>El consentimiento del <strong>Titular</strong> para el tratamiento de sus datos personales conforme a los términos de este Aviso se entiende otorgado cuando el <strong>Titular</strong>: (i) no manifiesta oposición alguna una vez que el presente Aviso haya sido puesto a su disposición; (ii) lo manifieste expresamente; y/o (iii) tenga una relación vigente con el <strong>Responsable</strong>. </p>

    <h2>IV. TRANSFERENCIAS.</h2>

    <p>El <strong>Responsable</strong> manifiesta que los datos personales del <strong>Titular</strong> no serán transferidos a terceros en ningún caso, salvo que la transferencia sea necesaria para el cumplimiento de las obligaciones derivadas de la relación jurídica que mantiene con el <strong>Titular</strong>; o bien, se actualice alguna de las excepciones previstas por el artículo 37 de la Ley Federal de Protección de Datos Personales en Posesión de los Particulares. </p>


    <h2>V.  MEDIDAS DE SEGURIDAD.</h2>

    <p>Capitala ha adoptado y mantiene las medidas de seguridad, administrativas, técnicas y físicas, necesarias para proteger sus datos personales contra daño, pérdida, alteración, destrucción o el uso, acceso o tratamiento no autorizado. Sin embargo, es claro que ninguna comunicación de datos por Internet es totalmente segura, por lo que Capitala no puede garantizar que sus datos personales estarán libres de todo daño, pérdida, alteración, destrucción o el uso, acceso o tratamiento no autorizado.</p>

    <h2>VI. DERECHOS DEL <strong>Titular</strong>.</h2>

    <p>El <strong>Titular</strong> tiene derecho a: (i) acceder a sus datos personales y conocer el tratamiento que se le da a los mismos; (iv) rectificar sus datos personales en los casos en que la información sea inexacta o incompleta; (v) cancelar sus datos personales cuando no se requieran para llevar a cabo las finalidades señaladas en el Aviso; se haya utilizado en contravención a lo dispuesto en el Aviso; y/o haya finalizado la relación por la cual fueron recabados; (vi) oponerse al tratamiento de sus datos personales para finalidades específicas; (vi) revocar su consentimiento al tratamiento de sus datos personales; y (vii) limitar el uso o divulgación de sus datos personales.</p>

    <p>Para hacer valer cualquiera de los derechos señalados en el párrafo anterior, el <strong>Titular</strong> deberá presentar una solicitud por escrito en el domicilio del <strong>Responsable</strong> o a la siguiente dirección electrónica <a href="mailto:info@capitala.mx">info@capitala.mx</a>, la cual deberá contener nombre y domicilio del <strong>Titular</strong>; documento que acredite la identidad del <strong>Titular</strong> o los documentos que acrediten la representación del mismo; una descripción clara y precisa de los datos personales respecto de los cuales busca ejercer alguno de los derechos señalados en el párrafo anterior; así como, cualquier otro elemento que facilite la identificación de los datos personales, y en su caso, exhibir la documentación que ampare la procedencia de las correcciones que solicita. </p>

    <p>La solicitud del <strong>Titular</strong> será atendida por el departamento de protección de datos personales del <strong>Responsable</strong>, el cual deberá dar contestación en un término de 20 (veinte) días hábiles contados a partir de la fecha de presentación de la solicitud. La contestación será enviada al domicilio o correo electrónico señalado por el <strong>Titular</strong>. En los casos en que el <strong>Titular</strong> ejercite su derecho de acceso a la información, el <strong>Responsable</strong> dará cumplimiento poniendo a disposición del <strong>Titular</strong> los datos personales, expidiendo copias simples, documentos electrónicos o cualquier otro medio que permita al <strong>Titular</strong> acceder a sus datos personales.</p>

    <h2>VII.  MODIFICACIONES. </h2>

    <p>El <strong>Responsable</strong> se reserva el derecho a efectuar en cualquier momento modificaciones y/o actualizaciones al Aviso, en atención a nuevas disposiciones legislativas y a nuevas políticas internas, mismas que estarán disponibles en el domicilio del <strong>Responsable</strong>.</p>

    <p>Así mismo, cualquier cambio realizado al presente Aviso de Privacidad, será comunicado mediante su publicación en nuestro sitio de internet <a href="http://www.capitala.mx">www.capitala.mx</a></p>


    <p>Fecha de publicación: Julio de 2017</p>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>
</html>
