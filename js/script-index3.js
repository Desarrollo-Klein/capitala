$(document).ready(function(){
    //Special script video for index video full    
    var onMobile = false;
    if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) { onMobile = true; }
    if( ( onMobile === false ) ) {
        $('.fullscreen-video').mb_YTPlayer({
            containment: "#page-wrap",
            mute: false,
            loop: false,
            startAt: 0,
            autoPlay: false,
            showYTLogo: false,
            showControls: false
        });
        $(".video-controls").hide()
        $("#video").on("YTPStart",function(){
            $(".video-controls.fullvideo").show().css({opacity: 1, visibility: "visible"});
            $("#owl-banner, .placeholder-before, .play.fullvideo").css({opacity: 0, visibility: "hidden"});
        });
        $("#video").on("YTPPause",function(){
            $(".play.fullvideo").css({opacity: 1, visibility: "visible"});
            $(".video-controls.fullvideo").css({opacity: 0, visibility: "hidden"});
        });
        $("#video").on("YTPEnd",function(){
            $(".video-controls.fullvideo, .placeholder-before").hide();
            $("#owl-banner, .placeholder-end, .play.fullvideo").show().css({opacity: 1, visibility: "visible", display: "block"});
        });

    }
});